const Course = require("../models/Course");



// Create a new course
/*
	Steps:
		1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
		2. Save the new Course to the database
*/



//activity 39

module.exports.addCourse = (reqBody, isAdmin) => {

	if(isAdmin) {

	let newCourse = new Course ({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price	

	});

	return newCourse.save().then(course => true).catch(err => false);

} else {
	return Promise.reject("Not Authorized");
}

};

// Retrieve all courses
/*
	Steps:
	1. Retrieve all the courses from the database
*/


 
 module.exports.getAllCourses = () => {

 	return Course.find({}).then(result => result)
 	.catch(err => err);

 }


 module.exports.getAllActive = () => {
 	return Course.find({isActive:true}).then(result=>result)
 	.catch(err=>err);
 };

//mini activity

module.exports.getCourse = (reqParams) => {

 	return Course.findById(reqParams.courseId).then(result=>result)
 	.catch(err=>err);
 };




// Update a course
/*
	Steps:
		1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
		2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/
// Information to update a course will be coming from both the URL parameters and the request body


module.exports.updateCourse = (reqParams, reqBody) => {

// Specify the fields/properties of the document to be updated


	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	/*
		Syntax:
			modelName.findByIdAndUpdate(documentId, updatesToBeApplied).then(statement)

	*/

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
	.then(course => true).catch(err => err);
}


// =============== Activity 40

module.exports.updateArchiveCourse = (reqParams, reqBody) => {


	let updatedCourse = {

		isActive: reqBody.isActive
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
	.then(course => true).catch(err => err);
}


// ================== END