const bcrypt = require("bcrypt");

// The "User" variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
const auth = require ("../auth");
const User = require ("../models/Users");
const Course = require("../models/Course");

// Check if the email already exists
/*
			Steps: 
			1. Use mongoose "find" method to find duplicate emails
			2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/


module.exports.checkEmailExists = (reqBody) => {

	// The result is sent back to the frontend via the "then" method found in the route file
	return User.find({email : reqBody.email}).then(result => {

	// The "find" method returns a record if a match is found
		if(result.length > 0){

			return true;

		// No duplicate email found
		// The user is not yet registered in the database	
		} else {
			
			return false;
		}
	}).catch(err => err)
};


// User registration
	/*
		Steps:
			1. Create a new User object using the mongoose model and the information from the request body
			2. Make sure that the password is encrypted
			3. Save the new User to the database
	*/

module.exports.registerUser	= (reqBody) => {


	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information

	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,

		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password : bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then(user =>{
		if (user){
			return true
		}else {
			return false
		}
	}).catch(err => err)
};



// User authentication
/*
	Steps:
		1. Check the database if the user email exists
		2. Compare the password provided in the login form with the password stored in the database
		3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/


module.exports.loginUser = (reqBody) => {

	// The "findOne" method returns the first record in the collection that matches the search criteria
	// We use the "findOne" method instead of the "find" method which returns all records that match the search criteria
	return User.findOne({email : reqBody.email}).then(result => {

		//User does not exist
		if(result == null) {
			return false;

		// User Exists
		}else {

			// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){

				return {access : auth.createAccesToken(result)}

				//passwords do not match
			}else {

				return false;
			}
		}
	}).catch(err => err);
}

// ACTIVITY s38


module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

	
		if(result == null) {
			return false;

		}else {
	
			result.password = ""						
				return result;
			}

	}).catch(err => err);
}





// Enroll user to a course
	/*
		Steps:
			1. Find the document in the database using the user's ID
			2. Add the course ID to the user's enrollment array
			3. Update the document in the MongoDB Atlas Database
	*/
// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user


module.exports.enroll = async (data) => {

	// Add the course ID in the enrollments array of the user
	// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
	// Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the frontend
	let isUserUpdated = await User.findById(data.userId)
		.then(user => {

			// Adds the courseId in the user's enrollments array
			user.enrollments.push({ courseId : data.courseId });

			// Saves the updated user information in the database
			return user.save().then(user => true)
				.catch(err => false)
		});

	// Add the user ID in the enrollees array of the course
	// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend
	let isCourseUpdated = await Course.findById(data.courseId)
		.then(course => {
			// Adds the userId in the course's enrollees array
			course.enrollees.push({ userId : data.userId });

			// Saves the updated course information in the database
			return course.save().then(course => true)
				.catch(err => false);
		});

	// Condition that will check if the user and course documents have been updated
	// User enrollment successful
	if(isUserUpdated && isCourseUpdated){
		return true;
	// User enrollment failure 
	} else {
		return false;
	}
}

